@extends('layouts.load')









@section('content')



            <div class="content-area">



              <div class="add-product-content1">

                <div class="row">

                  <div class="col-lg-12">

                    <div class="product-description">

                      <div class="body-area">

                        @include('includes.admin.form-error')

                    <form id="geniusformdata" action="{{route('admin-order-update',$data->id)}}" method="POST" enctype="multipart/form-data">

                        {{-- {{csrf_field()}} --}}
                        @csrf_field

                        {{-- Start Editing Length --}}
                        <div class="row">

                            <div class="col-lg-4">

                              <div class="left-area">

                                  <h4 class="heading">{{ __('Length') }} *</h4>

                              </div>

                            </div>

                            <div class="col-lg-7">

                              <input type="text" class="input-field" name="length" placeholder="{{ __('Enter Length Here') }}">

                            </div>

                        </div>

                        {{-- Start Editing Breadth --}}

                          <div class="row">

                            <div class="col-lg-4">

                              <div class="left-area">

                                  <h4 class="heading">{{ __('Breadth') }} *</h4>

                              </div>

                            </div>

                            <div class="col-lg-7">

                              <input type="text" class="input-field" name="breadth" placeholder="{{ __('Enter Breadth Here') }}">

                            </div>

                        </div>

                        {{-- Start Editing Height --}}
                          <div class="row">

                            <div class="col-lg-4">

                              <div class="left-area">

                                  <h4 class="heading">{{ __('Height') }} *</h4>

                              </div>

                            </div>

                            <div class="col-lg-7">

                              <input type="text" class="input-field" name="height" placeholder="{{ __('Enter Weight Here') }}">

                            </div>

                          </div>

                          {{-- Start Editing Weight --}}

                        <div class="row">

                          <div class="col-lg-4">

                            <div class="left-area">

                                <h4 class="heading">{{ __('Weight') }} *</h4>

                            </div>

                          </div>

                          <div class="col-lg-7">

                            <input ty class="input-field" name="weight" placeholder="{{ __('Enter Weight Here') }}"></textarea>

                          </div>

                        </div>


                        <br>

                        <div class="row">

                          <div class="col-lg-4">

                            <div class="left-area">



                            </div>

                          </div>

                          <div class="col-lg-7">

                            <button class="addProductSubmit-btn" type="submit">{{ __('Update') }}</button>

                          </div>

                        </div>

                    </form>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>



@endsection



@section('scripts')

@endsection



