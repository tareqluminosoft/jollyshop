@extends('layouts.admin')



@section('styles')



<style type="text/css">



.input-field {

    padding: 15px 20px;

}



</style>



@endsection



@section('content')



<input type="hidden" id="headerdata" value="{{ __('ORDER') }}">



                    <div class="content-area">

                        <div class="mr-breadcrumb">

                            <div class="row">

                                <div class="col-lg-6">

                                        <h4 class="heading">{{ __('All Orders') }}</h4>

                                        <ul class="links">

                                            <li>

                                                <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>

                                            </li>

                                            <li>

                                                <a href="javascript:;">{{ __('Orders') }}</a>

                                            </li>

                                            <li>

                                                <a href="{{ route('ship-order-index') }}">{{ __('All Orders') }}</a>

                                            </li>

                                        </ul>

                                </div>
                            <div class="col-lg-6">
                                <a href="{{ route('export-ship-order') }}" class="mybtn1"><i class="fa fa-shopping-cart"></i> {{ __('Export Order') }}</a>
                                <a href="#" class="mybtn1"><i class="fa fa-shopping-cart"></i> {{ __('Edit Customer Address') }}</a>

                            </div>





                        </div>

                        <div class="product-area">

                            <div class="row">

                                <div class="col-lg-12">

                                    <div class="mr-table allproduct">

                                        @include('includes.admin.form-success')

                                        <div class="table-responsiv">

                                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>

                                                <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">

                                                        <tr>

                                                            <th>{{ __('Track ID') }}</th>

                                                            <th>{{ __('Order Date') }}</th>

                                                            <th>{{ __('Channel') }}</th>

                                                            <th>{{ __('Order ID') }}</th>

                                                            <th>{{ __('Payment') }}</th>

                                                            <th>{{ __('Pickup Location') }}</th>

                                                            <th>{{ __('Product Details') }}</th>

                                                            <th>{{ __('Customer Details') }}</th>

                                                            <th>{{ __('Shipping Details') }}</th>

                                                            <th>{{ __('Action ') }}</th>

                                                        </tr>

                                                    @foreach ($results['data'] as $key=>$item)

                                                        <tr>
                                                            <td>{{ $item['id'] }}</td>
                                                            <td>{{ $item['channel_created_at'] }}</td>
                                                            <td>{{ $item['channel_id'] }}<br/>
                                                                {{ $item['channel_name'] }}<br/>
                                                                {{ $item['status'] }}
                                                            </td>
                                                            <td>{{ $item['channel_order_id'] }}</td>
                                                            <td>{{ $item['total'] }}</td>
                                                            <td>{{ $item['pickup_location'] }}</td>
                                                            <td>
                                                                @foreach($item['products'] as $key=> $product)
                                                                 {{ $product['name'] }} <br/>
                                                                 {{ $product['quantity'] }} <br/>
                                                                 {{ $product['price'] }}
                                                                @endforeach
                                                            </td>
                                                            <td>{{ $item['customer_name'] }}<br/>
                                                                {{ $item['customer_email'] }}<br/>
                                                                {{ $item['customer_phone'] }}<br/>
                                                                {{ $item['customer_address'] }}
                                                            </td>
                                                            <td>
                                                                @foreach($item['shipments'] as $key=>$ship)
                                                                {{ $ship['id'] }}<br/>
                                                                {{ $ship['dimensions'] }} <br/>
                                                                {{ $ship['weight'] }} Kg
                                                                @endforeach
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('get-ship-curier',$item['id']) }}" class="mybtn1"><i class="fa fa-shopping-cart"></i> {{ __('Ship') }}</a>
                                                                {{-- <button class="btn btn-primary" href="{{ route('get-ship-curier',$item['id']) }}">Ship</button> --}}
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                </table>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        {{-- <div class="product-area">

                            <div class="row">

                                <div class="col-lg-12">

                                    <div class="mr-table allproduct">

                                        @include('includes.admin.form-success')

                                        <div class="table-responsiv">

                                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>

                                                <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">

                                                </table>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div> --}}

                    </div>



{{-- ORDER MODAL --}}



{{-- <div class="modal fade" id="confirm-delete1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">

  <div class="modal-dialog">

    <div class="modal-content">

        <div class="submit-loader">

            <img  src="{{asset('assets/images/'.$gs->admin_loader)}}" alt="">

        </div>

    <div class="modal-header d-block text-center">

        <h4 class="modal-title d-inline-block">{{ __('Update Status') }}</h4>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                <span aria-hidden="true">&times;</span>

            </button>

    </div>



      <!-- Modal body -->

      <div class="modal-body">

        <p class="text-center">{{ __("You are about to update the order's Status.") }}</p>

        <p class="text-center">{{ __('Do you want to proceed?') }}</p>

        <input type="hidden" id="t-add" value="{{ route('admin-order-track-add') }}">

        <input type="hidden" id="t-id" value="">

        <input type="hidden" id="t-title" value="">

        <textarea class="input-field" placeholder="Enter Your Tracking Note (Optional)" id="t-txt"></textarea>

      </div>



      <!-- Modal footer -->

      <div class="modal-footer justify-content-center">

            <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>

            <a class="btn btn-success btn-ok order-btn">{{ __('Proceed') }}</a>

      </div>



    </div>

  </div>

</div> --}}



{{-- ORDER MODAL ENDS --}}







{{-- MESSAGE MODAL --}}

{{-- <div class="sub-categori">

    <div class="modal" id="vendorform" tabindex="-1" role="dialog" aria-labelledby="vendorformLabel" aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="vendorformLabel">{{ __('Send Email') }}</h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span>

                        </button>

                </div>

            <div class="modal-body">

                <div class="container-fluid p-0">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="contact-form">

                                <form id="emailreply">

                                    {{csrf_field()}}

                                    <ul>

                                        <li>

                                            <input type="email" class="input-field eml-val" id="eml" name="to" placeholder="{{ __('Email') }} *" value="" required="">

                                        </li>

                                        <li>

                                            <input type="text" class="input-field" id="subj" name="subject" placeholder="{{ __('Subject') }} *" required="">

                                        </li>

                                        <li>

                                            <textarea class="input-field textarea" name="message" id="msg" placeholder="{{ __('Your Message') }} *" required=""></textarea>

                                        </li>

                                    </ul>

                                    <button class="submit-btn" id="emlsub" type="submit">{{ __('Send Email') }}</button>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            </div>

        </div>

    </div>

</div> --}}



{{-- MESSAGE MODAL ENDS --}}



{{-- ADD / EDIT MODAL --}}



                {{-- <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">



                    <div class="modal-dialog modal-dialog-centered" role="document">

                        <div class="modal-content">

                                                <div class="submit-loader">

                                                        <img  src="{{asset('assets/images/'.$gs->admin_loader)}}" alt="">

                                                </div>

                                            <div class="modal-header">

                                            <h5 class="modal-title"></h5>

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                                <span aria-hidden="true">&times;</span>

                                            </button>

                                            </div>

                                            <div class="modal-body">



                                            </div>

                                            <div class="modal-footer">

                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>

                                            </div>

                        </div>

                    </div>



                </div> --}}



{{-- ADD / EDIT MODAL ENDS --}}



@endsection



@section('scripts')



{{-- DATA TABLE --}}



    {{-- <script type="text/javascript">



        var table = $('#geniustable').DataTable({

               ordering: false,

               processing: true,

               serverSide: true,

               ajax: '{{ route('ship-order-details','none') }}',

               columns: [

                        { data: 'created_at', name: 'customer_email' },

                        { data: 'id', name: 'id' },

                        { data: 'totalQty', name: 'totalQty' },

                        { data: 'pay_amount', name: 'pay_amount' },

                        { data: 'action', searchable: false, orderable: false }

                     ],

               language : {

                    processing: '<img src="{{asset('assets/images/'.$gs->admin_loader)}}">'

                },

                drawCallback : function( settings ) {

                        $('.select').niceSelect();

                }

            });



    </script> --}}



{{-- DATA TABLE --}}



@endsection
