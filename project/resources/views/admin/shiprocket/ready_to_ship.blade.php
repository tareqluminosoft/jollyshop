@extends('layouts.admin')



@section('styles')



<style type="text/css">



.input-field {

    padding: 15px 20px;

}



</style>



@endsection



@section('content')



<input type="hidden" id="headerdata" value="{{ __('ORDER') }}">



                    <div class="content-area">

                        <div class="mr-breadcrumb">

                            <div class="row">

                                <div class="col-lg-6">

                                        <h4 class="heading">{{ __('Ready to Ship Orders') }}</h4>

                                        <ul class="links">

                                            <li>

                                                <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>

                                            </li>

                                            <li>

                                                <a href="javascript:;">{{ __('Orders') }}</a>

                                            </li>

                                            <li>

                                                <a href="{{ route('ready-to-ship') }}">{{ __('All Orders') }}</a>

                                            </li>

                                        </ul>

                                </div>
                            <div class="col-lg-6">
                                <a href="{{ route('export-ship-order') }}" class="mybtn1"><i class="fa fa-shopping-cart"></i> {{ __('Export Order') }}</a>

                            </div>





                        </div>

                        <div class="product-area">

                            <div class="row">

                                <div class="col-lg-12">

                                    <div class="mr-table allproduct">

                                        @include('includes.admin.form-success')

                                        <div class="table-responsiv">

                                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>

                                                <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">

                                                        <tr>

                                                            <th>{{ __('Track ID') }}</th>

                                                            <th>{{ __('Order Date') }}</th>

                                                            <th>{{ __('Channel') }}</th>

                                                            <th>{{ __('Order ID') }}</th>

                                                            <th>{{ __('Payment') }}</th>

                                                            <th>{{ __('Pickup Location') }}</th>

                                                            <th>{{ __('Product Details') }}</th>

                                                            <th>{{ __('Customer Details') }}</th>

                                                            <th>{{ __('Shipping Details') }}</th>

                                                            <th>{{ __('Action ') }}</th>

                                                        </tr>

                                                    @foreach ($results['data'] as $key=>$item)
                                                        @if($item['status'] == "READY TO SHIP")
                                                        <tr>
                                                            <td>{{ $item['id'] }}</td>
                                                            <td>{{ $item['channel_created_at'] }}</td>
                                                            <td>{{ $item['channel_id'] }}<br/>
                                                                {{ $item['channel_name'] }}<br/>
                                                                {{ $item['status'] }}
                                                            </td>
                                                            <td>{{ $item['channel_order_id'] }}</td>
                                                            <td>{{ $item['total'] }}</td>
                                                            <td>{{ $item['pickup_location'] }}</td>
                                                            <td>
                                                                @foreach($item['products'] as $key=> $product)
                                                                 {{ $product['name'] }} <br/>
                                                                 {{ $product['quantity'] }} <br/>
                                                                 {{ $product['price'] }}
                                                                @endforeach
                                                            </td>
                                                            <td>{{ $item['customer_name'] }}<br/>
                                                                {{ $item['customer_email'] }}<br/>
                                                                {{ $item['customer_phone'] }}<br/>
                                                                {{ $item['customer_address'] }}
                                                            </td>
                                                            <td>
                                                                @foreach($item['shipments'] as $key=>$ship)
                                                                {{ $ship['id'] }}<br/>
                                                                {{ $ship['dimensions'] }} <br/>
                                                                {{ $ship['weight'] }} Kg
                                                                @endforeach
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-primary" href="#">Ship</button>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                    @endforeach

                                                </table>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>



                    </div>


@endsection



@section('scripts')



@endsection
