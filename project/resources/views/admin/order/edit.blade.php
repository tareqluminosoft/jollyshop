<html>
    <head>
<style>
    .form-control {
    display: block;
    width: 100%;
    height: calc(1.5em + 0.75rem + 2px);
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .button{
    border-radius: 50px;
    background-color: #1f224f;
    -webkit-box-shadow: 0px 5px 10px 0px rgb(14 35 107 / 30%);
    box-shadow: 0px 5px 10px 0px rgb(14 35 107 / 30%);
    text-align: center;
    color: #fff;
    font-size: 14px;
    font-weight: 400;
    display: inline-block;
    -webkit-transition: all 0.3s ease-in;
    -o-transition: all 0.3s ease-in;
    transition: all 0.3s ease-in;
    position: relative;
    padding: 7px 25px;
    border: 0px;
}

.container {
    max-width: 500px;
    margin: 50px auto;
    text-align: left;
    font-family: sans-serif;
}
form {
    border: 1px solid #1A33FF;
    background: #ecf5fc;
    padding: 40px 50px 45px;
}
.form-control:focus {
    border-color: #000;
    box-shadow: none;
}
label {
    font-weight: 600;
}
.error {
    color: red;
    font-weight: 400;
    display: block;
    padding: 6px 0;
    font-size: 14px;
}
.form-control.error {
    border-color: red;
    padding: .375rem .75rem;
}

</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
<body>

<div class="container mt-5">
    <h4 class="heading">
        Edit Product Dimention here
    </h4><br/>
        <form action="{{route('admin-dimention-update',$data->id)}}" method="POST" enctype="multipart/form-data">

            {{csrf_field()}}
            {{-- @csrf_field --}}

            {{-- Start Editing Length --}}

            {{-- @csrf --}}
            @method('PUT')

                <div class="form-group">
                    <label>Length</label>
                        {{-- <h4 class="heading">{{ __('Length') }} *</h4> --}}
                    <input type="number" class="form-control" name="length" value="{{ $data->length }}" placeholder="{{ __('Enter Length Here') }}">
                    </div>
                    <div class="form-group">
                    {{-- <h4 class="heading">{{ __('Breadth') }} *</h4> --}}
                    <label>Breadth</label>
                    <input type="number" class="form-control" name="breadth" value="{{ $data->breadth }}" placeholder="{{ __('Enter Breadth Here') }}">
                    </div>
                    <div class="form-group">
                    {{-- <h4 class="heading">{{ __('Height') }} *</h4> --}}
                    <label>Height</label>
                    <input type="number" class="form-control" name="height" value="{{ $data->height }}" placeholder="{{ __('Enter Height Here') }}">
                    </div>
                    <div class="form-group">
                    {{-- <h4 class="heading">{{ __('Weight') }} *</h4> --}}
                    <label>Weight</label>
                    <input type="number" class="form-control" name="weight" value="{{ $data->weight }}" placeholder="{{ __('Enter Weight Here') }}">
                    </div>

                    <br>

                    <button class="updatebutton" type="submit">{{ __('Update') }}</button>

        </form>

</div>
</body>
</html>
