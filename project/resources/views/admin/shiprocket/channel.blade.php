@extends('layouts.admin')



@section('styles')



<style type="text/css">



.input-field {

    padding: 15px 20px;

}



</style>



@endsection



@section('content')



<input type="hidden" id="headerdata" value="{{ __('ORDER') }}">



                    <div class="content-area">

                        <div class="mr-breadcrumb">

                            <div class="row">

                                <div class="col-lg-6">

                                        <h4 class="heading">{{ __('All Orders') }}</h4>

                                        <ul class="links">

                                            <li>

                                                <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>

                                            </li>

                                            <li>

                                                <a href="javascript:;">{{ __('Orders') }}</a>

                                            </li>

                                            <li>

                                                <a href="{{ route('ship-channel') }}">{{ __('All Channels') }}</a>

                                            </li>

                                        </ul>

                                </div>





                        </div>

                        <div class="product-area">

                            <div class="row">

                                <div class="col-lg-12">

                                    <div class="mr-table allproduct">

                                        @include('includes.admin.form-success')

                                        <div class="table-responsiv">

                                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>

                                                <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">

                                                        <tr>

                                                            <th>{{ __('Channel ID') }}</th>

                                                            <th>{{ __('Channel Name') }}</th>

                                                            <th>{{ __('Last Order Sync') }}</th>

                                                            <th>{{ __('Last Inventory Sync') }}</th>

                                                            <th>{{ __('Connection Status') }}</th>

                                                            <th>{{ __('Last Connection Sync') }}</th>

                                                            <th>{{ __('Status') }}</th>

                                                            <th>{{ __('Action ') }}</th>

                                                        </tr>

                                                    @foreach ($results['data'] as $key=>$item)

                                                        <tr>
                                                            <td>{{ $item['id'] }}</td>
                                                            <td>{{ $item['name'] }}</td>
                                                            <td>{{ $item['orders_synced_on'] }} </td>
                                                            <td>{{ $item['inventory_synced_on'] }}</td>
                                                            <td>{{ $item['connection'] }}</td>
                                                            <td>{{ $item['channel_updated_at'] }}</td>
                                                            <td>
                                                                {{ $item['status'] }}
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-primary" href="#">Edit</button>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                </table>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>



                    </div>



@endsection



@section('scripts')



@endsection
