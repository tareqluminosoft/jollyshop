<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class APIController Extends Controller
{
    public function pushOrder($id)
    {
        $getResult = Order::pushOrder($id);
        return response()->json(['status'==$getResult['status'],'message'=>$getResult['message']]);
    }
}
